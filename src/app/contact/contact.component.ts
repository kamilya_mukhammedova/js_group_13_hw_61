import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  modalOpen = false;
  @ViewChild('userNumberInput') userNumberInput!: ElementRef;
  userNumberLength = 0;

  constructor(private router: Router) {}

  openModalWindow() {
    this.modalOpen = true;
  }

  closeModalWindow() {
    this.modalOpen = false;
  }

  onUserNumber() {
    this.userNumberLength = this.userNumberInput.nativeElement.value.length;
  }

  sendNumber() {
    if(this.userNumberLength >= 9) {
      void this.router.navigate(['/userNumber']);
    } else{
      alert('Your number does be longer');
    }
  }
}
