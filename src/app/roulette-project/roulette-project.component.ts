import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { RouletteService } from '../shared/roulette.service';

@Component({
  selector: 'app-roulette-project',
  templateUrl: './roulette-project.component.html',
  styleUrls: ['./roulette-project.component.css']
})
export class RouletteProjectComponent implements OnInit {
  numbersArray: number[] = [];
  balance = 100;
  creditInput = 1;
  colorCredit = 'red';
  @ViewChild('redInput') redInput!: ElementRef;

  constructor(private rouletteService: RouletteService) {}

  ngOnInit() {
    this.numbersArray = this.rouletteService.getNumbers();
    this.rouletteService.newNumber.subscribe((number: number[]) => {
      this.numbersArray = number;
      const color = this.rouletteService.getColor(this.numbersArray[this.numbersArray.length - 1]);
      if(color === this.colorCredit) {
        this.balance += this.creditInput;
      } else if(color!== 'unknown' &&  color !== this.colorCredit) {
        this.balance -= this.creditInput;
      }
      if((color === 'zero') && (this.colorCredit === 'zero')) {
        this.balance += this.creditInput * 35;
      }
      if(color === 'unknown') {
        this.balance -= 0;
      }
    });
  }

  onStart() {
    this.rouletteService.start();
  }

  onStop() {
    this.rouletteService.stop();
  }

  onReset() {
    this.rouletteService.reset();
    this.balance = 100;
    this.creditInput = 1;
    this.redInput.nativeElement.checked = true;
    this.colorCredit = 'red';
  }

  getUserCredit(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.creditInput = parseInt(target.value);
  }

  onColorCredit(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.colorCredit = target.value;
  }
}
