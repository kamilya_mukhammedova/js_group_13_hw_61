import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { RouletteService} from '../shared/roulette.service';

@Directive({
  selector: '[appColorChange]'
})
export class ColorChangeDirective {
  colorClass = '';
  @Input() set appColorChange(element: string) {
     const neededColor = this.rouletteService.getColor(parseInt(element));
     if(neededColor === 'black') {
       this.colorClass = 'bg-dark';
     } else if(neededColor === 'red') {
       this.colorClass = 'bg-danger';
     } else if(neededColor === 'zero') {
       this.colorClass = 'bg-success';
     } else if(neededColor === 'unknown') {
       return;
     }
     this.renderer.addClass(this.el.nativeElement, this.colorClass);
  };

  constructor(private el: ElementRef, private renderer: Renderer2, private rouletteService: RouletteService) {}
}
