import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FormsModule } from '@angular/forms';
import { TodoProjectComponent } from './todo-project/todo-project.component';
import { ToDoListComponent } from './todo-project/to-do-list/to-do-list.component';
import { RouletteProjectComponent } from './roulette-project/roulette-project.component';
import { RouletteService } from './shared/roulette.service';
import { ColorChangeDirective } from './directives/color-change.directive';
import { GalleryProjectComponent } from './gallery-project/gallery-project.component';
import { FormsComponent } from './gallery-project/forms/forms.component';
import { NumbersComponent } from './gallery-project/numbers/numbers.component';
import { ContactComponent } from './contact/contact.component';
import { RouterModule, Routes } from '@angular/router';
import { ModalComponent } from './ui/modal/modal.component';
import { UserContactComponent } from './user-contact.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'todo', component: TodoProjectComponent},
  {path: 'roulette', component: RouletteProjectComponent},
  {path: 'gallery', component: GalleryProjectComponent},
  {path: 'contact', component: ContactComponent},
  {path: '**', component: UserContactComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    TodoProjectComponent,
    ToDoListComponent,
    RouletteProjectComponent,
    ColorChangeDirective,
    GalleryProjectComponent,
    FormsComponent,
    NumbersComponent,
    ContactComponent,
    ModalComponent,
    UserContactComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [RouletteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
