import { Component } from '@angular/core';

@Component({
  selector: 'app-user-contact',
  template: `
    <div class="alert alert-warning mt-5">
      <h1>Thank you for your contact!</h1>
    </div>`
})
export class UserContactComponent {}
